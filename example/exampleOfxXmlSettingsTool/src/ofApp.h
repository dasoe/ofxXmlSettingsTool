#pragma once

#include "ofMain.h"
#include "ofxXmlSettingsTool.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		void keyPressed(int key);

		ofxXmlSettingsTool settingsXml,  otherXml;

};
