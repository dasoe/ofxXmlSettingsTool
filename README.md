# ofxXmlSettingsTool

A simple tool for basic setting needs. Easy settings (one level depth only) for the following data types:

- boolean 
- string 
- int
- float
- color
- vec2f
- vec3f
- vec4f

With optional auto save and required flag as well as debug/verbose settings and respective output.
Please see example for how to use.
